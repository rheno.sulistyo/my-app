FROM node:18

WORKDIR /usr/src/app
COPY app.js /usr/src/app
EXPOSE 8081
CMD [ "node", "app.js" ]