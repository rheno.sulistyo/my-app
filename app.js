var http = require("http");

http.createServer(function (request, response) {
   // Send the HTTP header 
   // HTTP Status: 200 : OK
   // Content Type: text/plain
   response.writeHead(200, {'Content-Type': 'text/plain'});
   
   // Send the response body as "Hello World"
   response.end('Hello World !!!\n\nTesting for image updater again and again !\n\nTesting Again !\n\nAgain and again !!!\n\nCoba\n\nLorem Ipsum !!!');
}).listen(8081);

var today = new Date();
const obj = {date: today, status: "ok", message: "App is Running"};
const myJSON = JSON.stringify(obj);

setInterval(()=>{
      console.log(myJSON);
   }, 10000);

// Console will print the message
console.log('app is running');
